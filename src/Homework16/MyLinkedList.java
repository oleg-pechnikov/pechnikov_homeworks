package Homework16;

public class MyLinkedList <T> {

    // Домашнее задание:

    public T get(int index){
        Node<T> current = first;
        if(size/2 > index) {
            while (index == size){
                current = current.next;
                size++;
            }
        }
        return current.value;
    }



    // Стандартный код из лекции

    // Вложенный класс Узел - объект списка хранящий значение
    private static class Node<T>{
        T value;
        Node<T> next;

        public Node(T value) {
            this.value = value;
        }
    }

    private Node<T> first;
    private Node<T> last;

    private int size;

    public void add(T element){
        Node <T> newNode = new Node<>(element);
        if(size == 0){
            first = newNode;
        } else {
            last.next = newNode;
        }
        last = newNode;
        size++;
    }
    public void addToBegin(T element){
        Node <T> newNode = new Node<>(element);
        if(size == 0){
            last = newNode;
        } else {
            newNode.next = first;
        }
        first = newNode;
        size++;
    }


    public int size(){
        return size;
    }



}
