package Homework161;

public interface MyList <E> {

    void removeAt(int index);

    void add(E element);

    E get(int index);

    void clear();





}
