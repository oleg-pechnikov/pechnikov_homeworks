package Homework161;

public class MyLinkedList<T> implements MyList {

    // Домашнее задание:
    @Override
    public T get(int index) {
        // Проверка на корректность
        if (index < size && index >= 0) {
            //Смотрим с какой стороны списка ближе индекс, который ищем
            if (size / 2 > index) {
                //Создаём ссылку на голову
                Node<T> current = first;
                //Проходим фором по списку
                for (int i = 0; i < index; i++) {
                    current = current.next;
                    //выводим значение
                    return (T) current.value;
                }
            } else {
                //Создаём ссылку на хвост
                Node<T> current = last;
                //Проходим фором по списку
                for (int i = size - 1; i > index; i--) {
                    current = current.prev;
                    //выводим значение
                    return (T) current.value;
                }
            }
        }
        //Если вышли за пределы выводим предупреждение
        System.err.println("Вышли за пределы списка");
        return null;
    }




    // Стандартный код

    private static class Node<T> {
        T value;
        Node<T> next;
        Node<T> prev;

        public Node(T value) {
            this.value = value;
        }
    }

    private Node<T> first;
    private Node<T> last;
    private int size;

    @Override
    public void add(Object element) {
        Node<T> newNode = (Node<T>) new Node<>(element);
        if (size == 0) {
            first = newNode;
        } else {
            last.next = newNode;
        }
        last = newNode;
        size++;
    }

    public void addToBegin(T element) {
        Node<T> newNode = new Node<>(element);
        if (size == 0) {
            last = newNode;
        } else {
            newNode.next = first;
        }
        first = newNode;
        size++;
    }

    public int size() {
        return size;
    }

    @Override
    public void removeAt(int index) {

    }

    @Override
    public void clear() {

    }


}
