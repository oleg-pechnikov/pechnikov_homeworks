package Homework161;

import java.util.Arrays;
import java.util.Iterator;

public class MyArrayList<T> implements MyList, Iterable<T> {

    // Домашнее задание

    @Override
    public void removeAt(int index) {
        for (int i = index; i < size; i++){
            elements[i] = elements[i + 1];
        }
        size--;
    }



    //Стандартные методы из лекции
    private static final int DEFAULT_SIZE = 10;
    private int size;
    private T[] elements;

    public MyArrayList() {
        this.elements = (T[]) new Object[DEFAULT_SIZE];
        size = 0;
    }
    @Override
    public void add(Object element) {
        if (size == elements.length) {
            T[] oldElements = this.elements;
            this.elements = (T[]) new Object[oldElements.length + oldElements.length / 2];
            for (int i = 0; i < size; i++) {
                this.elements[i] = oldElements[i];
            }
        }
        this.elements[size] = (T) element;
        size++;
    }

    @Override
    public T get(int index) {
        if (index >= 0 && index < size) {
            return elements[index];
        } else {
            return null;
        }
    }

    @Override
    public void clear() {
        size = 0;
    }

    @Override
    public String toString() {
        return "MyArrayList{" +
                "size=" + size +
                ", elements=" + Arrays.toString(elements) +
                '}';
    }


    // Реализация Iterator by Iterable
    // Можно использовать Foreach
    private class MyArrayListIterator implements Iterator<T>{
        private int current = 0;

        @Override
        public boolean hasNext() {
            return current < size;
        }

        @Override
        public T next() {
            T value = elements[current];
            current++;
            return value;
        }
    }

    @Override
    public Iterator<T> iterator() {
        return null;
    }
}
