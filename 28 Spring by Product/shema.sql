create table goods
(
    id          serial primary key,
    product_name varchar(20) not null,
    price        double precision check (price >= 0 ),
    quantity    integer check (quantity >= 0 )
);

create table customer
(
    id        serial primary key,
    first_name varchar(20),
    last_name  varchar(20),
    owner_id integer,
    foreign key (owner_id) references goods (id)
);


insert into goods(product_name, price, quantity) values ('Молоко', 50, 100);
insert into goods(product_name, price, quantity) values ('Хлеб', 40, 100);
insert into goods(product_name, price, quantity) values ('Мясо', 350, 500);
insert into goods(product_name, price, quantity) values ('Рыба', 250, 200);
insert into goods(product_name, price, quantity) values ('Орехи', 650, 50);

insert into customer(first_name, last_name) values ('Иван', 'Иванов');
insert into customer(first_name, last_name) values ('Пётр', 'Петров');
insert into customer(first_name, last_name) values ('Сергей', 'Сергеев');
insert into customer(first_name, last_name) values ('Константин', 'Константинов');




