package homework.spring_by_product.web.controller;

import homework.spring_by_product.web.forms.ProductForm;
import homework.spring_by_product.web.models.Customer;
import homework.spring_by_product.web.models.Product;
import homework.spring_by_product.web.repositories.ProductsRepository;
import homework.spring_by_product.web.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;


@Controller
public class ProductController {

    private final ProductService productService;

    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("/products")
    public String getProductsPage(Model model){
        List<Product> products = productService.getAllProducts();
        model.addAttribute("products", products);
        return "products";
    }

    @GetMapping("/products/{product-id}")
    public String getProductPage(Model model, @PathVariable("product-id") Integer productId){
        Product product = productService.getProduct(productId);
        model.addAttribute("product", product);
        return "product";
    }

    @PostMapping("/products")
    public String addProduct(ProductForm productForm){
        productService.addProduct(productForm);
        return "redirect:/products";
    }

    @PostMapping("/products/{product-id}/delete")
    public String deleteProduct(@PathVariable("product-id") Integer productId){
        productService.deleteProduct(productId);
        return "redirect:/products";
    }

    @PostMapping("/products/{product-id}/update")
    public String update(@PathVariable("product-id") Long productId){
        //productService.update(productId, ProductForm);
        return "redirect:/products";
    }

    @GetMapping("/products/{product-id}/customers")
    public String getCustomersByProducts(Model model, @PathVariable("product-id") Integer productId){
        List<Customer> customers = productService.getCustomersByProducts(productId);
        List<Customer> unproductCustomer = productService.getCustomerWithoutProduct();
        model.addAttribute("productId", productId);
        model.addAttribute("customers", customers);
        model.addAttribute("unproductCustomer", unproductCustomer);
        return "customers_of_product";
    }

    @PostMapping("/products/{product-id}/customers")
    public String addCustomerToProduct(@PathVariable("product-id") Integer productId, @RequestParam("customerId") Integer customerId){
        productService.addCustomerToProduct(productId, customerId);
        return "redirect:/products/" + productId + "/customers";
    }
}
