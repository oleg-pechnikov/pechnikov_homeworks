package homework.spring_by_product.web.models;

import lombok.*;

import javax.persistence.*;
import java.util.List;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "goods")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String productName;
    private Double price;
    private Integer quantity;

    @OneToMany(mappedBy = "owner")
    private List<Customer> customers;

}
