package homework.spring_by_product.web.services;

import homework.spring_by_product.web.forms.ProductForm;
import homework.spring_by_product.web.models.Customer;
import homework.spring_by_product.web.models.Product;

import java.util.List;

public interface ProductService {

    void addProduct(ProductForm productForm);
    List<Product> getAllProducts();

    void deleteProduct(Integer productId);

    Product getProduct(Integer productId);

    List<Customer> getCustomersByProducts(Integer productId);

    List<Customer> getCustomerWithoutProduct();

    void addCustomerToProduct(Integer productId, Integer customerId);
}
