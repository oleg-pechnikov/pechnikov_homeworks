package homework.spring_by_product.web.services;

import homework.spring_by_product.web.forms.ProductForm;
import homework.spring_by_product.web.models.Customer;
import homework.spring_by_product.web.models.Product;
import homework.spring_by_product.web.repositories.CustomersRepository;
import homework.spring_by_product.web.repositories.ProductsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@RequiredArgsConstructor
@Component
public class ProductServiceImpl implements ProductService {

    private final ProductsRepository productsRepository;
    private final CustomersRepository customersRepository;

//    @Autowired
//    public ProductServiceImpl(ProductsRepository productsRepository, CustomersRepository customersRepository) {
//        this.productsRepository = productsRepository;
//        this.customersRepository = customersRepository;
//    }

    @Override
    public void addProduct(ProductForm productForm) {
        Product product = Product.builder()
                .productName(productForm.getProductName())
                .price(productForm.getPrice())
                .quantity(productForm.getQuantity())
                .build();

        productsRepository.save(product);
    }

    @Override
    public List<Product> getAllProducts() {
        return productsRepository.findAll();
    }

    @Override
    public void deleteProduct(Integer productId) {
        productsRepository.deleteById(productId);

    }

    @Override
    public Product getProduct(Integer productId) {
        return productsRepository.getById(productId);
    }

    @Override
    public List<Customer> getCustomersByProducts(Integer productId) {
        return customersRepository.findAllByOwner_Id(productId);
    }

    @Override
    public List<Customer> getCustomerWithoutProduct() {
        return customersRepository.findAllByOwnerIsNull();
    }

    @Override
    public void addCustomerToProduct(Integer productId, Integer customerId) {
        Product product = productsRepository.getById(productId);
        Customer customer = customersRepository.getById(customerId);
        customer.setOwner(product);
        customersRepository.save(customer);
    }
}
