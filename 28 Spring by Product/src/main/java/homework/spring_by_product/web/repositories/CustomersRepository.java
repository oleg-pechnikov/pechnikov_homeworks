package homework.spring_by_product.web.repositories;

import homework.spring_by_product.web.models.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CustomersRepository extends JpaRepository<Customer, Integer> {

    List<Customer> findAllByOwner_Id(Integer Id);
    List<Customer> findAllByOwnerIsNull();
}
