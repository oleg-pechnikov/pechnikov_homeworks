package homework.spring_by_product.web.repositories;

import homework.spring_by_product.web.models.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductsRepository extends JpaRepository<Product, Integer> {
}
