//package homework.spring_by_product.web.repositories;
//
//import homework.spring_by_product.web.models.Product;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.jdbc.core.JdbcTemplate;
//import org.springframework.jdbc.core.RowMapper;
//import org.springframework.stereotype.Component;
//
//import javax.sql.DataSource;
//import java.util.List;
//
//@Component
//public class ProductsRepositoryJdbcTempImpl implements ProductsRepository {
//
//
//    //language=SQL
//    private static final String SQL_INSERT = "insert into goods(product_name, price, quantity) values (?,?,?)";
//    //language=SQL
//    private static final String SQL_SELECT_ALL = "select * from goods";
//    //language=SQL
//    private static final String SQL_SELECT_BY_PRICE = "select * from goods where price = ?;";
//    //language=SQL
//    private static final String SQL_DELETE_BY_ID = "delete from goods where id = ?;";
//    //language=SQL
//    private static final String SQL_SELECT_BY_ID = "select * from goods where id = ?";
//
//
//    private final JdbcTemplate jdbcTemplate;
//
//
//
//    @Autowired
//    public ProductsRepositoryJdbcTempImpl(DataSource dataSource) {
//        this.jdbcTemplate = new JdbcTemplate(dataSource);
//    }
//
//    @Override
//    public void save(Product product) {
//        jdbcTemplate.update(SQL_INSERT, product.getProductName(), product.getPrice(), product.getQuantity());
//
//    }
//
//    private static final RowMapper<Product> productRowMapper = (row, rowNumber) -> {
//        int id = row.getInt("id");
//        String productName = row.getString("product_name");
//        double price = row.getDouble("price");
//        int quantity = row.getInt("quantity");
//
//        return new Product(id, productName, price, quantity);
//    };
//
//    @Override
//    public List<Product> findAll() {
//        return jdbcTemplate.query(SQL_SELECT_ALL, productRowMapper);
//    }
//
//    @Override
//    public List<Product> findAllByPrice(double price) {
//        return jdbcTemplate.query(SQL_SELECT_BY_PRICE + price, productRowMapper);
//    }
//
//    @Override
//    public void delete(Long productId) {
//        jdbcTemplate.update(SQL_DELETE_BY_ID, productId);
//    }
//
//    @Override
//    public Product findById(Long productId) {
//        return jdbcTemplate.queryForObject(SQL_SELECT_BY_ID, productRowMapper, productId);
//    }
//}
