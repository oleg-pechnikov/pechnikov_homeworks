package homework.spring_by_product.web.forms;

import lombok.Data;

@Data
public class ProductForm {
    private String productName;
    private Double price;
    private Integer quantity;
}
